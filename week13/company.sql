create temporary table products_below_avg
select productid, productname, price
from Products
where price < (select avg(price) from Products);

drop table products_below_avg;

show table status;

select * from products_below_avg;

