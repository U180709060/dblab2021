-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema steamapp
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `steamapp` ;

-- -----------------------------------------------------
-- Schema steamapp
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `steamapp` DEFAULT CHARACTER SET utf8 ;
USE `steamapp` ;

-- -----------------------------------------------------
-- Table `categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `categories` ;

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` INT NOT NULL,
  `category_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`category_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `system_requirements`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `system_requirements` ;

CREATE TABLE IF NOT EXISTS `system_requirements` (
  `requirement_id` INT NOT NULL,
  `win_requirements` VARCHAR(255) NULL,
  `mac_requirements` VARCHAR(255) NULL,
  `linux_requirements` VARCHAR(255) NULL,
  PRIMARY KEY (`requirement_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `price`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `price` ;

CREATE TABLE IF NOT EXISTS `price` (
  `price_id` INT NOT NULL,
  `currency` VARCHAR(10) NOT NULL,
  `initial_price` REAL NOT NULL,
  `final_price` REAL NOT NULL,
  `discount_percent` INT NOT NULL,
  PRIMARY KEY (`price_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `platforms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `platforms` ;

CREATE TABLE IF NOT EXISTS `platforms` (
  `platform_id` INT NOT NULL,
  `support_win` TINYINT NOT NULL,
  `support_mac` TINYINT NOT NULL,
  `support_linux` TINYINT NOT NULL,
  PRIMARY KEY (`platform_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `genres`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `genres` ;

CREATE TABLE IF NOT EXISTS `genres` (
  `genre_id` INT NOT NULL,
  `genre_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`genre_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comment` ;

CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` INT NOT NULL,
  `positive_comment_num` INT NOT NULL,
  `negative_comment_num` INT NOT NULL,
  `recommendation_num` INT NOT NULL,
  `reviews` VARCHAR(255) NULL,
  PRIMARY KEY (`comment_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `release_date`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `release_date` ;

CREATE TABLE IF NOT EXISTS `release_date` (
  `release_id` INT NOT NULL,
  `release_day` INT NOT NULL,
  `release_month` VARCHAR(15) NOT NULL,
  `release_year` INT NOT NULL,
  PRIMARY KEY (`release_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `game`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `game` ;

CREATE TABLE IF NOT EXISTS `game` (
  `steam_app_id` INT NOT NULL,
  `category_id` INT NOT NULL,
  `requirement_id` INT NOT NULL,
  `price_price_id` INT NOT NULL,
  `platform_id` INT NOT NULL,
  `genre_id` INT NOT NULL,
  `comment_id` INT NOT NULL,
  `release_date_id` INT NOT NULL,
  `name` VARCHAR(150) NOT NULL,
  `required_age` INT NOT NULL,
  `is_free` TINYINT NOT NULL,
  `controller_support` VARCHAR(10) NULL,
  `about_the_game` VARCHAR(1000) NOT NULL,
  `supported_languages_num` INT NOT NULL,
  `website_link` VARCHAR(255) NULL,
  `developers` VARCHAR(150) NOT NULL,
  `publishers` VARCHAR(150) NOT NULL,
  `metacritic_score` INT NULL,
  `owners` VARCHAR(50) NULL,
  `achievements` INT NOT NULL,
  `content_descriptors` VARCHAR(255) NULL,
  PRIMARY KEY (`steam_app_id`, `category_id`, `requirement_id`, `price_price_id`, `platform_id`, `genre_id`, `comment_id`, `release_date_id`),
  CONSTRAINT `fk_game_categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `categories` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_system_requirements1`
    FOREIGN KEY (`requirement_id`)
    REFERENCES `system_requirements` (`requirement_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_price1`
    FOREIGN KEY (`price_price_id`)
    REFERENCES `price` (`price_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_platforms1`
    FOREIGN KEY (`platform_id`)
    REFERENCES `platforms` (`platform_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `genres` (`genre_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_comment1`
    FOREIGN KEY (`comment_id`)
    REFERENCES `comment` (`comment_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_release_date1`
    FOREIGN KEY (`release_date_id`)
    REFERENCES `release_date` (`release_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `categories`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `categories` (`category_id`, `category_name`) VALUES (2, '\"Single Player\"');

COMMIT;


-- -----------------------------------------------------
-- Data for table `system_requirements`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `system_requirements` (`requirement_id`, `win_requirements`, `mac_requirements`, `linux_requirements`) VALUES (1, '\"Minimum: 500 mhz processor, 96mb ram, 16mb video card, Windows XP, Mouse, Keyboard, Internet Connection, Recommended:800 mhz processor, 128mb ram, 32mb+ video card, Windows XP, Mouse, Keyboard, Internet Connection\"', '\"Minimum: OS X  Snow Leopard 10.6.3, 1GB RAM, 4GB Hard Drive Space,NVIDIA GeForce 8 or higher, ATI X1600 or higher, or Intel HD 3000 or higher Mouse, Keyboard, Internet Connection\"', '\"Minimum: Linux Ubuntu 12.04, Dual-core from Intel or AMD at 2.8 GHz, 1GB Memory, nVidia GeForce 8600/9600GT, ATI/AMD Radeaon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1, 4GB Hard Drive Space, OpenAL Compatible Sound Card\"');

COMMIT;


-- -----------------------------------------------------
-- Data for table `price`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `price` (`price_id`, `currency`, `initial_price`, `final_price`, `discount_percent`) VALUES (9, 'GBP', 3.99, 3.99, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `platforms`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (1, true, false, false);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (2, false, true, false);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (3, false, false, true);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (4, true, true, false);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (5, false, true, true);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (6, true, false, true);
INSERT INTO `platforms` (`platform_id`, `support_win`, `support_mac`, `support_linux`) VALUES (7, true, true, true);

COMMIT;


-- -----------------------------------------------------
-- Data for table `genres`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `genres` (`genre_id`, `genre_name`) VALUES (1, '\"Action\"');

COMMIT;


-- -----------------------------------------------------
-- Data for table `comment`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `comment` (`comment_id`, `positive_comment_num`, `negative_comment_num`, `recommendation_num`, `reviews`) VALUES (9, 3822, 420, 3351, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `release_date`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `release_date` (`release_id`, `release_day`, `release_month`, `release_year`) VALUES (9, 1, 'Jun', 2001);

COMMIT;


-- -----------------------------------------------------
-- Data for table `game`
-- -----------------------------------------------------
START TRANSACTION;
USE `steamapp`;
INSERT INTO `game` (`steam_app_id`, `category_id`, `requirement_id`, `price_price_id`, `platform_id`, `genre_id`, `comment_id`, `release_date_id`, `name`, `required_age`, `is_free`, `controller_support`, `about_the_game`, `supported_languages_num`, `website_link`, `developers`, `publishers`, `metacritic_score`, `owners`, `achievements`, `content_descriptors`) VALUES (130, 2, 1, 9, 7, 1, 9, 9, '\"Half-Life: Blue Shift\"', 0, False, NULL, 'Made by Gearbox Software and originally released in 2001 as an add-on to Half-Life, Blue Shift is a return to the Black Mesa Research Facility in which you play as Barney Calhoun, the security guard sidekick who helped Gordon out of so many sticky situations.', 3, NULL, '\"Gearbox Software\"', '\"Valve\"', 71, '5,000,000 .. 10,000,000', 0, NULL);

COMMIT;

