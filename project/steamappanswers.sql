use steamapp;
# Show the average recommendations group by developers.
select avg(recommendation_num), developers
from game join comment on comment.comment_id = game.comment_id
group by developers;

#Show the games which  price is USD.
select name
from game
where price_price_id in(
	select price_id
    from price
    where currency = "USD");
    
#Show the name of free games grouped by owner.
select name
from game
where is_free = true
group by owners;

#Show the games which positive comments is greater than 10000 and metacritic score is 90.
select name, metacritic_score
from game
where comment_id in(
	select comment_id
    from comment
    where positive_comment_num > 100000) and metacritic_score > 90;
    
#Show the games which is published by UBISOFT and released before 2010.
select name
from game
where release_date_id in(
	select release_id
    from release_date
    where release_year < 2010) and publishers = "Ubisoft";